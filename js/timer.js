chrome = chrome || {};
(function($){
  "use strict";

  var lastTimerReloading = 0;
  var bTimerReloads = false;
  var timers = {};

  /*
   * Универсальный get (firefox, chrome)
   */
  var fStorageGet = function(keys, onGot, onError) {
    if (!onGot) onGot = function(item){};
    if (!onError) onError = function(reason){};
    try {
      chrome.storage.local.get(keys).then(onGot, onError);
    } catch (err) {
      chrome.storage.local.get(keys, function(item) {
        if (chrome.runtime.lastError) {
          if (chrome.runtime.lastError.message) {
            onError(chrome.runtime.lastError.message);
          }
        }
        onGot(item);
      });
    }
  };

  /*
   * Универсальный set (firefox, chrome)
   */
  var fStorageSet = function(keys, onGot, onError) {
    if (!onGot) onGot = function(item){};
    if (!onError) onError = function(reason){};
    try {
      chrome.storage.local.set(keys).then(onGot, onError);
    } catch (err) {
      chrome.storage.local.set(keys, function(item) {
        if (chrome.runtime.lastError) {
          if (chrome.runtime.lastError.message) {
            onError(chrome.runtime.lastError.message);
          }
        }
        onGot(item);
      });
    }
  };

  /*
   * Обновление локальных таймеров из хранилища браузера.
   */
  var fReloadtimer = function(forced) {
    var currentTime = Math.floor($.now() / 1000);
    if (!bTimerReloads && (forced || lastTimerReloading !== currentTime)) {
      bTimerReloads = true;
      fStorageGet({timers: {}}, function(item) {
        timers = item.timers;
        bTimerReloads = false;
        lastTimerReloading = currentTime;
        $(window.document).trigger('jira-timer.reloaded');
      }, function(){
        bTimerReloads = false;
      });
    }
  };

  /*
   * Устанавливает изменения значений таймера с сохранением в storage.
   */
  var fSetTimer = function (keyName, values) {
    var onGot = function(item) {
      timers = item.timers;
      if (values === true) {
        delete(timers[keyName]);
      } else {
        if (!timers[keyName]) timers[keyName] = {};
        for (var k in values) {
          timers[keyName][k] = values[k];
        }
        if (timers[keyName].timing) {
          for (var k in timers) {
            if (k !== keyName && timers[k].timing) {
              timers[k].timing = false;
              timers[k].fixTime = timers[k].fixTime + $.now() - timers[k].time;
            }
          }
        }
      }
      fStorageSet({timers: timers});
    };
    var onError = function (error) {
      console.log("Error: ${error}");
    };
    var promise = fStorageGet({timers: {}}, onGot, onError);
  };

  /*
   * Сбрасывает заданный таймер
   */
  var fFlushTimer = function (keyName) {
    fSetTimer(keyName, true);
  };

  /*
   * Возвращает часы, минуты и секунды из количества миллисекунд.
   */
  var fHourMinSec = function (ms) {
    var h = Math.floor(ms / 3600000);
    var m = Math.floor((ms % 3600000) / 60000);
    var s = Math.floor((ms % 60000) / 1000);
    return [h, m, s];
  };

  /*
   * Преобразует %time в строке в читаемое время из миллисекунд.
   */
  var fTime2String = function (str, ms) {
    var ar = fHourMinSec(ms), h = ar[0], m = ar[1], s = ar[2];
    return str.replace('%time', (h > 0 ? (h > 9 ? '' : '0') + h + ':' : '') +
        (m > 9 ? '' : '0') + m + ':' + (s > 9 ? '' : '0') + s);
  };

  /*
   * Преобразует миллисекунды в формат для формы логирования.
   */
  var fTime2HourMinStr = function (ms) {
    var ar = fHourMinSec(ms + 30000), h = ar[0], m = ar[1];
    return (h > 0 ? h + 'h ' : '') + (m > 0 ? m + 'm' : '');
  };

  setInterval(function(){
    fReloadtimer();
  }, 50);


  /*
   * Работа со страницей формы сохранения worklog.
   */
  $(function(){
    var $inputTime = $('#log-work-time-logged');
    var $inputDate = $('#log-work-date-logged-date-picker');
    var $cancelLink = $('#log-work-cancel');
    var $submitButton = $('#log-work-submit');
    if ($inputTime.length < 1 || $inputDate.length < 1 || $cancelLink.length < 1) return;

    var keyName = $cancelLink.prop('href').replace(/^.*\/browse\/(.+)$/, '$1');

    var fUpdateTime = function(){
      if ($inputTime.val() === '' && timers[keyName] && !timers[keyName].timing) {
        $inputTime.val(fTime2HourMinStr(timers[keyName].fixTime));
      }
    };

    var fClickLog = function() {
      if (timers[keyName] && $inputTime.val() !== '' && $inputDate.val() !== '') {
        fFlushTimer(keyName);
      }
    };

    $(window.document).on('jira-timer.reloaded', fUpdateTime);
    $submitButton.click(fClickLog);
  });

  /*
   * Работа со страницей задачи.
   */
  $(function(){
    if ($('#issuedetails').length < 1 || $('#opsbar-opsbar-timer').length > 0) return;

    var fullTitle = window.document.title;
    var keyName = fullTitle.replace(/^\[#(.*?)\].*$/, '$1');

    // Создание и получение основных jQuery-объектов, с которыми придётся работать
    var $wrapper = $('#stalker');
    var $lastMenu = $wrapper.find('.ops-menus').children('[id^=opsbar]').last();
    $lastMenu.after('<ul id="opsbar-opsbar-timer" class="ops">' +
      '<li><a class="button issueaction-timer first" data-label="start" href="#">&#9205; Start</a></li>' +
      '<li><a class="button issueaction-timer disabled" data-label="save" href="#">&#128190; Save</a></li>' +
      '<li><a class="button issueaction-timer disabled last" data-label="flush" href="#">&#10060; Flush</a></li>' +
      '</ul>');
    var $timerMenu = $('#opsbar-opsbar-timer'), $links = $('#opsbar-opsbar-timer a');

    // Хранилище состояний кнопок
    var linkLabels = {
      start: ['&#9205; Start', 'Click to start timing.'],
      pause: ['&#9208; %time', 'Click to pause timing.'],
      continue: ['&#9205; %time', 'Click to continue timing.'],
      save: ['&#128190; Save', 'Click to save your work log time.'],
      flush: ['&#10060; Flush', 'Click to flush your work log time.']
    };

    /*
     * Заменяет у ссылки текст и обработчик события, либо отключает ее.
     */
    var fChangeLink = function (link, label, disabled) {
      var $link = $(link);
      if (!label) label = $link.data('label');
      var text = linkLabels[label][0];
      var title = linkLabels[label][1];
      if (timers[keyName]) {
        var ms = (timers[keyName].timing ? $.now() - timers[keyName].time : 0) + timers[keyName].fixTime;
        text = fTime2String(text, ms);
        title = fTime2String(title, ms);
      } else {
        text = text.replace(/\s*\(%time\)\s*/, ' ');
        title = title.replace(/\s*\(%time\)\s*/, ' ');
      }
      $link.prop('innerHTML', text);
      $link.attr('title', title);
      $link.data('label', label);
      if (disabled === true) {
        $link.addClass('disabled').css({cursor:'default'});
      } else {
        $link.removeClass('disabled').css({cursor:'pointer'});
      }
    };

    /*
     * Обработчик кнопок
     */
    var fButtonClick = function() {
//      console.log('f-' + $(this).data('label'));
      if ($(this).hasClass('disabled')) return false;
      lastTimerReloading = 0;
      switch ($(this).data('label')) {
        case 'start':
          if (!timers[keyName] || !timers[keyName].timing) {
            fSetTimer(keyName, {timing: true, time: $.now(), fixTime: 0});
          }
          break;
        case 'continue':
          if (timers[keyName] || !timers[keyName].timing) {
            fSetTimer(keyName, {timing: true, time: $.now()});
          }
          break;
        case 'pause':
          if (timers[keyName] || timers[keyName].timing) {
            var newTime = timers[keyName].fixTime + $.now() - timers[keyName].time;
            fSetTimer(keyName, {timing: false, fixTime: newTime});
            //fUpdateWorklogLink(newTime);
          }
          break;
        case 'save':
          if (timers[keyName] || !timers[keyName].timing) {
            //fUpdateWorklogLink();
            fOpenWorklog();
          }
          break;
        case 'flush':
          if (timers[keyName] || !timers[keyName].timing) {
            if (confirm('Do you really want to flush all progress?')) {
              fFlushTimer(keyName);
            }
          }
          break;
      }
      return false;
    };

    /*
     * Обновляет переменную timeLogged в ссылке на worklog
     */
    var fUpdateWorklogLink = function(newTime) {
      if (!newTime) newTime = timers[keyName].fixTime;
      /*$('#log-work').prop('href',
        $('#log-work').prop('href').replace(/&timeLogged=(\d+h)?\+?(\d+m)?/, '') + '&timeLogged=' + fTime2HourMinStr(newTime)
      );*/
    };

    /*
     * Открыватель записи в worklog
     */
    var fOpenWorklog = function() {
      location.href=$('#log-work').prop('href');
    };



    /*
     * Коррекция ссылок в зависимости от реального состояния таймера.
     */
    var fCorrectLinks = function() {
      if (timers[keyName]) {
        if (timers[keyName].timing) {
          if ($($links[0]).data('label') !== 'pause') {
            fChangeLink($links[1], null, true);
            fChangeLink($links[2], null, true);
          }
          fChangeLink($links[0], 'pause');
        } else {
          if ($($links[0]).data('label') !== 'continue') {
            fChangeLink($links[0], 'continue');
            fChangeLink($links[1]);
            fChangeLink($links[2]);
          }
        }
      } else {
        if ($($links[0]).data('label') !== 'start') {
          fChangeLink($links[0], 'start');
          fChangeLink($links[1], null, true);
          fChangeLink($links[2], null, true);
        }
      }
    };

    $links.on('click.timerFunc', fButtonClick);
    $(window.document).on('jira-timer.reloaded', fCorrectLinks);

  });
})(jQuery);