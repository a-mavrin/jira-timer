# README #

JIRA Timer

### What is this repository for? ###

Plugin for JIRA 4.3.4 adds timer for work logging

This repository: https://bitbucket.org/a-mavrin/jira-timer

Firefox: https://addons.mozilla.org/ru/firefox/addon/jira-timer/

Chrome: https://chrome.google.com/webstore/detail/jira-timer/bnmfaakendaopcllceebgaammjphbbek
